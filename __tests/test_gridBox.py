import _imports
import pytest
from pybi.core.components import GridBoxComponent


def test_areas_array2str():
    input = [["sc1", "sc2"], ["sc3"], ["table"] * 4]
    act = GridBoxComponent.areas_array2str(input)

    exp = '"sc1 sc2 . ." "sc3 . . ." "table table table table"'

    assert act == exp


def test_areas_str2array():
    input = """
    sc1 sc2
    sc3
    table table table table
"""
    act = GridBoxComponent.areas_str2array(input)

    exp = [["sc1", "sc2"], ["sc3"], ["table", "table", "table", "table"]]

    assert act == exp


def test_areas_str2array_with_space():
    input = """
    sc1  sc2
    sc3
    table  table    table table
"""
    act = GridBoxComponent.areas_str2array(input)

    exp = [["sc1", "sc2"], ["sc3"], ["table", "table", "table", "table"]]

    assert act == exp


def test_padded_grid_template():
    sizes = ["1fr"]
    real_size = 3
    act = GridBoxComponent.padded_grid_template(sizes, real_size)

    exp = ["1fr", "auto", "auto"]
    assert act == exp


def test_padded_grid_template_empty():
    sizes = []
    real_size = 3
    act = GridBoxComponent.padded_grid_template(sizes, real_size)

    exp = ["auto", "auto", "auto"]
    assert act == exp


def test_padded_grid_template_not_pad():
    sizes = ["1fr", "1fr", "1fr"]
    real_size = 3
    act = GridBoxComponent.padded_grid_template(sizes, real_size)

    exp = ["1fr", "1fr", "1fr"]
    assert act == exp
