from __future__ import annotations
from typing import TYPE_CHECKING, List, Optional, Union


from pybi.utils.data_gen import Jsonable, get_global_id
from .componentTag import ComponentTag

if TYPE_CHECKING:
    from pybi.core.styles.styles import Style
    from pybi.core.sql import Sql


class Component(Jsonable):
    def __init__(self, tag: ComponentTag) -> None:
        self.id = get_global_id()
        self.tag = tag
        self._debugTag: Optional[str] = None
        self._styles: List[Style] = []
        self.visible: Union[bool, Sql] = True
        self.__gridArea: str = ""

    def set_debugTag(self, tag: str):
        self._debugTag = tag
        return self

    def set_gridArea(self, area_name: str):
        self.__gridArea = area_name
        return self

    def _to_json_dict(self):
        data = super()._to_json_dict()
        styles_dict = {}
        for style in self._styles:
            styles_dict.update(style._get_styles_dict())
        data["styles"] = styles_dict

        if isinstance(self.visible, bool) and self.visible == True:
            del data["visible"]

        if self.__gridArea:
            data["gridArea"] = self.__gridArea

        data["debugTag"] = self._debugTag

        return data

    def __add__(self, other: Style):
        self._styles.append(other)
        return self

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, self.__class__):
            return hash(__o.id) == hash(self.id)

        return False
