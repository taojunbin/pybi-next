import { Component } from "./component"



export interface ReactiveComponent extends Component {
    linkages: string[]
    sourceName: string | null
    updateInfos: {
        table: string
        field: string
    }[]

}

export interface SingleReactiveComponent extends ReactiveComponent {
    sqlInfo: {
        sql: string
    }
}



export interface Slicer extends SingleReactiveComponent {
    multiple: boolean
    title: string
    field: string
}


export interface Table extends SingleReactiveComponent {

}


export type TChartType = 'bar'

export type TEchartUpdateInfo = {
    actionType: 'hover' | 'click'
    valueType: 'x' | 'y' | 'value'
    table: string
    field: string
}


export type TEChartType = 'line' | 'pie' | 'bar'

export interface EChart extends ReactiveComponent {
    width: string
    height: string
    options: { series: {}[] }
    sqlInfos: {
        seriesConfig: { type: string },
        path: string
        dataset: string
    }[]
    updateInfos: TEchartUpdateInfo[]
    mapping: {
        type: TChartType
        x: string
        y: string
        color: string
    }
}



export type TTextValueSqlInfo = {
    sql: string
}

export interface TextValue extends SingleReactiveComponent {
    contexts: (string | TTextValueSqlInfo)[]
}