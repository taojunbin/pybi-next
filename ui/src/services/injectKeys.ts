import { InjectionKey } from "vue";
import { TComponentServices } from "./componentServices";
import { TDatasetServices } from "./datasetServices";
import { TSqlAnalyzeService } from "./sqlAnalyzeServices";
import { TDbServices } from "./dbServices";


export const sqlAnalyzeKey = Symbol() as InjectionKey<TSqlAnalyzeService>
export const componentKey = Symbol() as InjectionKey<TComponentServices>
export const datasetKey = Symbol() as InjectionKey<TDatasetServices>
export const dbKey = Symbol() as InjectionKey<TDbServices>