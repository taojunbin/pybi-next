
import { TCpId } from "@/models/types";
import { Ref } from "vue";



export interface IDataset {
    typeName: 'dataSource' | 'dataView' | 'pivot-dataView'
    name: string
    toSqlWithFilters: (requestorId: string) => Ref<string>,
    addFilter: (cpid: TCpId, expression: string) => void,
    removeFilters: (cpid: TCpId) => void,

    initFilter: (cpid: TCpId) => void,
}


export interface IDataSource extends IDataset {

}

export interface IDataView extends IDataset {
    sql: string
    cp2FilterMap: Map<string, Ref<string>>
    addLinkageDataset: (dataset: IDataset) => void
}

export interface IPivotDataView extends IDataset {
    sourceDatasetName: string

    addLinkageDataset: (dataset: IDataset) => void
}