import { createApp, App } from 'vue'
import './style.css'
import AppComponent from './App.vue'
import 'element-plus/dist/index.css'


const app = createApp(AppComponent)

if (import.meta.env.PROD) {
    app.mount('#app')
} else {
    import('element-plus').then((ElementPlus) => {
        app.use(ElementPlus).mount('#app')
    })

}

// }


